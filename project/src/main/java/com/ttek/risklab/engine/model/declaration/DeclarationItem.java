package com.ttek.risklab.engine.model.declaration;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DeclarationItem implements java.io.Serializable {

    static final long serialVersionUID = 1L;
    private String uuidReference;
    private String reference;
    private int lineSeq;
    private String declarationHeaderReference;
    private Integer totalItems;
    private String packageTypeCode;
    private String packageTypeName;
    private String hsCode;
    private String goodsDescription;
    private String countryOfOriginCode;
    private BigDecimal grossWeight;
    private String grossWeightUnitMeasurementCode;
    private BigDecimal netWeight;
    private String netWeightUnitMeasurementCode;
    private BigDecimal itemPrice;
    private BigDecimal itemNumber;
    private String itemPriceCurrencyCode;
    private BigDecimal commercialValue;
    private String commercialCurrencyCode;
    private BigDecimal insuranceValue;
    private String insuranceCurrencyCode;
    private BigDecimal freightValue;
    private String freightCurrencyCode;
    private BigDecimal cifValue;
    private String cifCurrencyCode;
    private List<String> containerNumbers;
    private String billOfLadingReference;
    private List<String> containerReferences;
    private Date insertedAtTimestamp;
    private Date updatedAtTimestamp;
    private Boolean isHsCodeVerify;
    public DeclarationItem() {
    }

    public String getUuidReference() {
        return uuidReference;
    }

    public String getReference() {
        return reference;
    }

    public int getLineSeq() {
        return lineSeq;
    }

    public String getDeclarationHeaderReference() {
        return declarationHeaderReference;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public String getPackageTypeCode() {
        return packageTypeCode;
    }

    public String getPackageTypeName() { return packageTypeName; }

    public String getHsCode() {
        return hsCode;
    }

    public String getGoodsDescription() {
        return goodsDescription;
    }

    public String getCountryOfOriginCode() {
        return countryOfOriginCode;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public String getGrossWeightUnitMeasurementCode() {
        return grossWeightUnitMeasurementCode;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public String getNetWeightUnitMeasurementCode() {
        return netWeightUnitMeasurementCode;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public BigDecimal getItemNumber() { return itemNumber; }

    public String getItemPriceCurrencyCode() {
        return itemPriceCurrencyCode;
    }

    public BigDecimal getCommercialValue() {
        return commercialValue;
    }

    public String getCommercialCurrencyCode() {
        return commercialCurrencyCode;
    }

    public BigDecimal getInsuranceValue() {
        return insuranceValue;
    }

    public String getInsuranceCurrencyCode() {
        return insuranceCurrencyCode;
    }

    public BigDecimal getFreightValue() {
        return freightValue;
    }

    public String getFreightCurrencyCode() {
        return freightCurrencyCode;
    }

    public BigDecimal getCifValue() {
        return cifValue;
    }

    public String getCifCurrencyCode() {
        return cifCurrencyCode;
    }

    public List<String> getContainerNumbers() {
        return containerNumbers;
    }

    public String getBillOfLadingReference() {
        return billOfLadingReference;
    }

    public List<String> getContainerReferences() {
        return containerReferences;
    }

    public Boolean getHsCodeVerify() {
        return isHsCodeVerify;
    }

    public Date getInsertedAtTimestamp() { return insertedAtTimestamp; }

    public Date getUpdatedAtTimestamp() { return updatedAtTimestamp; }

}