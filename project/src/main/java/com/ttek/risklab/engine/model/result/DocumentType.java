package com.ttek.risklab.engine.model.result;

public enum DocumentType {
    DeclarationHeader,
    DeclarationItem,
    DECLARATION_PACKAGES,
    DECLARATION_CONTAINER,
    DECLARATION_PARTY,

    MANIFEST_HEADER,
    MANIFEST_ITEM,

    BILL_OF_LADING_HEADER,
    BILL_OF_LADING_ITEM,
    BILL_OF_LADING_CARGO,
}
