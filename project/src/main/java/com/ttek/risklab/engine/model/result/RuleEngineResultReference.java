package com.ttek.risklab.engine.model.result;



import java.util.List;

public class RuleEngineResultReference {
    private String ruleId;
    private String threatRuleSet;
    private List<RuleEngineResultDetail> ruleHitReferences;

    public RuleEngineResultReference(){

    }

    public RuleEngineResultReference(String ruleId, String threatRuleSet, List<RuleEngineResultDetail> ruleHitReferences) {
        this.ruleId = ruleId;
        this.threatRuleSet = threatRuleSet;
        this.ruleHitReferences = ruleHitReferences;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public void setThreatRuleSet(String threatRuleSet) {
        this.threatRuleSet = threatRuleSet;
    }

    public void setRuleHitReferences(List<RuleEngineResultDetail> ruleHitReferences) {
        this.ruleHitReferences = ruleHitReferences;
    }

    public String getRuleId() {
        return ruleId;
    }

    public String getThreatRuleSet() {
        return threatRuleSet;
    }

    public List<RuleEngineResultDetail> getRuleHitReferences() {
        return ruleHitReferences;
    }

    public static class RuleEngineResultReferenceBuilder {
        private String ruleId;
        private String threatRuleSet;
        private List<RuleEngineResultDetail> ruleHitReferences;

        RuleEngineResultReferenceBuilder() {
        }

        public RuleEngineResultReference.RuleEngineResultReferenceBuilder ruleId(String ruleId) {
            this.ruleId = ruleId;
            return this;
        }

        public RuleEngineResultReference.RuleEngineResultReferenceBuilder threatRuleSet(String threatRuleSet) {
            this.threatRuleSet = threatRuleSet;
            return this;
        }

        public RuleEngineResultReference.RuleEngineResultReferenceBuilder ruleHitReferences(List<RuleEngineResultDetail> ruleHitReferences) {
            this.ruleHitReferences = ruleHitReferences;
            return this;
        }

        public RuleEngineResultReference build() {
            return new RuleEngineResultReference(this.ruleId, this.threatRuleSet, this.ruleHitReferences);
        }

        public String toString() {
            return "RuleEngineResultReference.RuleEngineResultReferenceBuilder(ruleId=" + this.ruleId + ", threatRuleSet=" + this.threatRuleSet + ", ruleHitReferences=" + this.ruleHitReferences + ")";
        }
    }
}