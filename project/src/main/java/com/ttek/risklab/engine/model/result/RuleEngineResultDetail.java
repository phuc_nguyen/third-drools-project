package com.ttek.risklab.engine.model.result;



public class RuleEngineResultDetail implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    public RuleEngineResultDetail() {
    }
    
    private String documentReference;
    private DocumentType documentType;
    private String elementName;
    private String elementValue;

   RuleEngineResultDetail(String documentReference, DocumentType documentType, String elementName, String elementValue) {
        this.documentReference = documentReference;
        this.documentType = documentType;
        this.elementName = elementName;
        this.elementValue = elementValue;
    }

    public static RuleEngineResultDetail.RuleEngineResultDetailBuilder builder() {
        return new RuleEngineResultDetail.RuleEngineResultDetailBuilder();
    }

    public void setDocumentReference(String documentReference) {
        this.documentReference = documentReference;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public void setElementValue(String elementValue) {
        this.elementValue = elementValue;
    }

    public String getDocumentReference() {
        return this.documentReference;
    }

    public DocumentType getDocumentType() {
        return this.documentType;
    }

    public String getElementName() {
        return this.elementName;
    }

    public String getElementValue() {
        return this.elementValue;
    }

    public String toString() {
        return "RuleEngineResultDetail(documentReference=" + this.getDocumentReference() + ", documentType=" + this.getDocumentType() + ", elementName=" + this.getElementName() + ", elementValue=" + this.getElementValue() + ")";
    }

    public static class RuleEngineResultDetailBuilder {
        private String documentReference;
        private DocumentType documentType;
        private String elementName;
        private String elementValue;

        RuleEngineResultDetailBuilder() {
        }

        public RuleEngineResultDetail.RuleEngineResultDetailBuilder documentReference(String documentReference) {
            this.documentReference = documentReference;
            return this;
        }

        public RuleEngineResultDetail.RuleEngineResultDetailBuilder documentType(DocumentType documentType) {
            this.documentType = documentType;
            return this;
        }

        public RuleEngineResultDetail.RuleEngineResultDetailBuilder elementName(String elementName) {
            this.elementName = elementName;
            return this;
        }

        public RuleEngineResultDetail.RuleEngineResultDetailBuilder elementValue(String elementValue) {
            this.elementValue = elementValue;
            return this;
        }

        public RuleEngineResultDetail build() {
            return new RuleEngineResultDetail(this.documentReference, this.documentType, this.elementName, this.elementValue);
        }

        public String toString() {
            return "RuleEngineResultDetail.RuleEngineResultDetailBuilder(documentReference=" + this.documentReference + ", documentType=" + this.documentType + ", elementName=" + this.elementName + ", elementValue=" + this.elementValue + ")";
        }
    }
}