package com.ttek.risklab.engine.model.result;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ttek.risklab.engine.model.declaration.Declaration;
import com.ttek.risklab.engine.model.declaration.DeclarationItem;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DeclarationRuleEngineResult implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    public DeclarationRuleEngineResult() {
    }
    private Declaration declaration;
    private DeclarationItem declarationItem;
    private List<String> declarationFieldNames;
    private List<String> declarationItemFieldNames;
    private String ruleId;
    private String trsId;


    public void setDeclaration(Declaration declaration) {
        this.declaration = declaration;
    }

    public void setDeclarationItem(DeclarationItem declarationItem) {
        this.declarationItem = declarationItem;
    }

    public void setDeclarationFieldNames(List<String> declarationFieldNames) {
        this.declarationFieldNames = declarationFieldNames;
    }

    public void setDeclarationItemFieldNames(List<String> declarationItemFieldNames) {
        this.declarationItemFieldNames = declarationItemFieldNames;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public void setTrsId(String trsId) {
        this.trsId = trsId;
    }

    public Declaration getDeclaration() {
        return this.declaration;
    }

    public DeclarationItem getDeclarationItem() {
        return this.declarationItem;
    }

    public List<String> getDeclarationFieldNames() {
        return this.declarationFieldNames;
    }

    public List<String> getDeclarationItemFieldNames() {
        return this.declarationItemFieldNames;
    }

    public String getRuleId() {
        return this.ruleId;
    }

    public String getTrsId() {
        return this.trsId;
    }

    public DeclarationRuleEngineResult(Declaration declaration, DeclarationItem declarationItem, List<String> declarationFieldNames, List<String> declarationItemFieldNames, String ruleId, String trsId) {
        this.declaration = declaration;
        this.declarationItem = declarationItem;
        this.declarationFieldNames = declarationFieldNames;
        this.declarationItemFieldNames = declarationItemFieldNames;
        this.ruleId = ruleId;
        this.trsId = trsId;
    }



}