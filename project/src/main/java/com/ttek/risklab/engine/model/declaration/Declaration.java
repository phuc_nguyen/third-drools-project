package com.ttek.risklab.engine.model.declaration;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Declaration implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    public Declaration() {
    }
    private String uuidReference;
    private String reference;
    private Integer regimeCode;
    private String regimeName;
    private Integer transportCode;
    private String transportName;
    private String exporterName;
    private String exporterReference;
    private String exporterAddress;
    private String importerName;
    private String importerReference;
    private String importerAddress;
    private String consignorName;
    private String consignorAddress;
    private String consignorReference;
    private String consigneeName;
    private String consigneeAddress;
    private String consigneeReference;
    private String exportCountryCode;
    private String importCountryCode;
    private String portOfLoadingCode;
    private String portOfDischargeCode;
    private String portOfLoadingName;
    private BigDecimal totalCommercialValue;
    private String commercialCurrencyCode;
    private BigDecimal totalInsuranceValue;
    private String insuranceCurrencyCode;
    private BigDecimal totalFreightValue;
    private String freightCurrencyCode;
    private BigDecimal totalGrossWeight;
    private String grossWeightUnitMeasurementCode;
    private BigDecimal totalNetWeight;
    private String netWeightUnitMeasurementCode;
    private String manifestReference;
    private Date arrivalTimestamp;
    private Integer totalContainers;
    private Integer totalLines;
    private Integer totalPackages;
    private String paymentMethod;
    private Date insertedAtTimestamp;
    private Date updatedAtTimestamp;
    private List<DeclarationItem> declarationItems;

    public String getUuidReference() {
        return uuidReference;
    }

    public String getReference() {
        return reference;
    }

    public Integer getRegimeCode() {
        return regimeCode;
    }

    public String getRegimeName() { return regimeName; }

    public Integer getTransportCode() {
        return transportCode;
    }

    public String getTransportName() { return transportName; }

    public String getExporterName() {
        return exporterName;
    }

    public String getExporterReference() {
        return exporterReference;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public String getImporterName() {
        return importerName;
    }

    public String getImporterReference() {
        return importerReference;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public String getConsignorReference() {
        return consignorReference;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public String getConsigneeReference() {
        return consigneeReference;
    }

    public String getExportCountryCode() {
        return exportCountryCode;
    }

    public String getImportCountryCode() {
        return importCountryCode;
    }

    public String getPortOfLoadingCode() {
        return portOfLoadingCode;
    }

    public String getPortOfLoadingName() { return portOfLoadingName; }

    public String getPortOfDischargeCode() {
        return portOfDischargeCode;
    }

    public BigDecimal getTotalCommercialValue() {
        return totalCommercialValue;
    }

    public String getCommercialCurrencyCode() {
        return commercialCurrencyCode;
    }

    public BigDecimal getTotalInsuranceValue() {
        return totalInsuranceValue;
    }

    public String getInsuranceCurrencyCode() {
        return insuranceCurrencyCode;
    }

    public BigDecimal getTotalFreightValue() {
        return totalFreightValue;
    }

    public String getFreightCurrencyCode() {
        return freightCurrencyCode;
    }

    public BigDecimal getTotalGrossWeight() {
        return totalGrossWeight;
    }

    public String getGrossWeightUnitMeasurementCode() {
        return grossWeightUnitMeasurementCode;
    }

    public BigDecimal getTotalNetWeight() {
        return totalNetWeight;
    }

    public String getNetWeightUnitMeasurementCode() {
        return netWeightUnitMeasurementCode;
    }

    public String getManifestReference() {
        return manifestReference;
    }

    public Date getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public Integer getTotalContainers() {
        return totalContainers;
    }

    public Integer getTotalLines() {
        return totalLines;
    }

    public Integer getTotalPackages() {
        return totalPackages;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public Date getInsertedAtTimestamp() {
        return insertedAtTimestamp;
    }

    public Date getUpdatedAtTimestamp() {
        return updatedAtTimestamp;
    }

    public List<DeclarationItem> getDeclarationItems() {
        return declarationItems;
    }
}