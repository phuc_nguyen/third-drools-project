package com.ttek.risklab.engine.model.result;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.Collections;

public class RuleEngineResult implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    public RuleEngineResult() {
    }

    private List<RuleEngineResultReference> riskRuleHits = new ArrayList<>();

    public void add(DeclarationRuleEngineResult declarationResult){
        
        List<RuleEngineResultDetail> ruleEngineResultDetails = new ArrayList();
        if(declarationResult.getDeclarationFieldNames() != null && !"".equals(declarationResult.getDeclarationFieldNames())){
            Map<String, Object> declarationMap = (Map)(new ObjectMapper()).convertValue(declarationResult.getDeclaration(), Map.class);
            declarationResult.getDeclarationFieldNames().stream().forEach((item) -> {
                RuleEngineResultDetail ruleEngineResultDetail = RuleEngineResultDetail.builder().documentReference(String.valueOf(declarationMap.get("uuidReference"))).elementName(item).elementValue(String.valueOf(declarationMap.get(item))).documentType(DocumentType.DeclarationHeader).build();
                ruleEngineResultDetails.add(ruleEngineResultDetail);
            });
        }
        if(declarationResult.getDeclarationItemFieldNames() != null && !"".equals(declarationResult.getDeclarationItemFieldNames())){
            Map<String, Object> declarationItemMap = (Map)(new ObjectMapper()).convertValue(declarationResult.getDeclarationItem(), Map.class);
            declarationResult.getDeclarationItemFieldNames().stream().forEach((item) -> {
                RuleEngineResultDetail ruleEngineResultDetail = RuleEngineResultDetail.builder().documentReference(String.valueOf(declarationItemMap.get("uuidReference"))).elementName(item).elementValue(String.valueOf(declarationItemMap.get(item))).documentType(DocumentType.DeclarationItem).build();
                ruleEngineResultDetails.add(ruleEngineResultDetail);
            });
        }
        
        RuleEngineResultReference ruleEngineResultReference = new RuleEngineResultReference.RuleEngineResultReferenceBuilder().ruleHitReferences(ruleEngineResultDetails).ruleId(declarationResult.getRuleId()).threatRuleSet(declarationResult.getTrsId()).build();
        
        this.riskRuleHits.add(ruleEngineResultReference);
    }

    public void setRiskRuleHits(List<RuleEngineResultReference> ruleEngineResultReferences){
        this.riskRuleHits = ruleEngineResultReferences;
    }

    public List<RuleEngineResultReference> getRiskRuleHit(){
       // this.riskRuleHits.stream().sorted(Comparator.comparing(RuleEngineResultReference::getRuleId)).collect(Collectors.toList());
       Collections.sort(this.riskRuleHits, new RiskComparator());
        return this.riskRuleHits;
    }
    
    public class RiskComparator implements Comparator<RuleEngineResultReference> {
    @Override
        public int compare(RuleEngineResultReference ref1, RuleEngineResultReference ref2) {
            return extractInt(ref1) - extractInt(ref2);
        }
        
        int extractInt(RuleEngineResultReference ref) {
            String num = ref.getRuleId().replaceAll("\\D", "");
            return num.isEmpty() ? 0 : Integer.parseInt(num);
        }
        
    }
}
